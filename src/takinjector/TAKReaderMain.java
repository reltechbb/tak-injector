/*
 * TAKReaderMain.java
 *
 * Copyright (c) 2001-2022 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Read messages from the TAK server
 *
 */
package takinjector;

/**
 *
 * @author jmerritt
 */
public class TAKReaderMain {
    static String ip = "10.0.100.105";
//    static String ip = "98.175.96.248";
    static String port = "8088";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TAKInjector injector = new TAKInjector ();
        injector.connect (ip, port);
        injector.receive();
        injector.disconnect ();
    }
}
