/*
 * TAKMessageBuilder.java
 *
 * Copyright (c) 2001-2022 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Builds COT messages to send to the TAK server
 * Simulates the PH messages
 *
 */
package takinjector;

import java.text.SimpleDateFormat;
import static java.time.ZoneOffset.UTC;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jmerritt
 */
public class TAKMessageBuilder  extends TAKInjector implements Runnable {
    private final TAKInjectorUI ui;
    private final String header = "<event version=\"2.0\" uid=\"messageuid\" type=\"y-f-G-U-C\" time=\"messagetime\" start=\"starttime\" stale=\"staletime\" how=\"m-g\">" ;
    private final String point = "<point lat=\"latitude\" lon=\"longitude\" hae=\"123.4\" ce=\"32.0\" le=\"32.0\"/>";
    private final String detailStart = "<detail>";
    private final String detailEnd = "</detail>";
    private final String readingStart = "<reading>";
    private final String readingEnd = "</reading>";
    private final String name = "<name>username</name>";
    private final String time = "<time>eventtime</time>";
    private final String metric = "<metric name=\"attributename\" value=\"attributevalue\" threshold=\"attributethreshold\" level=\"attributelevel\"/>";
    private final String metricNoXhold = "<metric name=\"attributename\" value=\"attributevalue\" level=\"attributelevel\"/>";
    private final String metricNoValue = "<metric name=\"attributename\" level=\"attributelevel\"/>";
    private final String metricOnlyValue = "<metric name=\"attributename\" value=\"attributevalue\"/>";
    private final String eventEnd = "</event>";
    private final String DTFORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private final Double maxLat = 38.90911;
    private final Double minLat = 38.90771;
    private final Double minLon = -77.4754;
    private final Double maxLon = -77.47282;
    private Double user1Lat = 38.90856;
    private Double user1Lon = -77.47373;
    private Double user1DeltaLat = 0.0003;
    private Double user1DeltaLon = 0.0003;
    private Double user2Lat = 38.90846;
    private Double user2Lon = -77.47363;
    private Double user2DeltaLat = -0.00025;
    private Double user2DeltaLon = 0.0003;
            
    public TAKMessageBuilder (TAKInjectorUI ui) {
        super ();
        this.ui = ui;
    }
    
    public Boolean buildAndSend () {
        Boolean result = true;
        if (ui.isUserEnabled1()) {
            result &= send (buildMessage1());
        }
        if (ui.isUserEnabled2()) {
            Integer count = ui.getNumberUsers();
            for (Integer i = 0; i < count; i++)
            {
                result &= send (buildMessage2(i));
            }
        }
        return result;
    }
    
    private String buildMessage1 () {
        StringBuilder message = new StringBuilder ();
        // user 1
        String temp = header.replace("messageuid", "BIOTRAC-22345");
        temp = temp.replace("messagetime", buildTime());
        temp = temp.replace("starttime", buildTime());
        temp = temp.replace("staletime", buildStaleTime());
        message.append(temp);
        // point
        temp = point.replace("latitude", user1Lat.toString());
        temp = temp.replace("longitude", user1Lon.toString());
        message.append(temp);
        if (ui.isMotionSelected1()) {
            // update lat and lon
            user1Lat += user1DeltaLat;
            if (user1Lat > maxLat || user1Lat < minLat) {
                user1DeltaLat = -user1DeltaLat;
            }
            // update lat and lon
            user1Lon += user1DeltaLon;
            if (user1Lon > maxLon || user1Lon < minLon) {
                user1DeltaLon = -user1DeltaLon;
            }
        }
        // detail
        message.append (detailStart);
        message.append (readingStart);
        // reading
        // time
        temp = time;
        temp = temp.replace("eventtime", buildTime());
        message.append (temp);
        // name
        temp = name;
        temp = temp.replace("username", "Fred Flintstone");
        message.append (temp);
        // metrics
        if (ui.isSendExertion1()) {
            temp = metric;
            temp = temp.replace("attributename", "exertion");
            if (!ui.isExertionAlert1()) {
                temp = temp.replace("attributevalue", "55.1");
                temp = temp.replace("attributethreshold", "95.1");
                temp = temp.replace("attributelevel", "GREEN");
            } else {
                temp = temp.replace("attributevalue", "97.1");
                temp = temp.replace("attributethreshold", "95.1");
                temp = temp.replace("attributelevel", "RED");                
            }
            message.append(temp);
        }
        if (ui.isSendHeartrate1()) {
            temp = metric;
            temp = temp.replace("attributename", "heartrate");
            if (!ui.isHeartrateAlert1()) {
                temp = temp.replace("attributevalue", "77.1");
                temp = temp.replace("attributethreshold", "180.1");
                temp = temp.replace("attributelevel", "GREEN");
            } else {
                temp = temp.replace("attributevalue", "185.1");
                temp = temp.replace("attributethreshold", "180.1");
                temp = temp.replace("attributelevel", "RED");                
            }
            message.append(temp);
        }
        if (ui.isSendCoretemp1()) {
            temp = metric;
            temp = temp.replace("attributename", "coretemp");
            if (!ui.isCoretempAlert1()) {
                temp = temp.replace("attributevalue", "98.1");
                temp = temp.replace("attributethreshold", "100.1");
                temp = temp.replace("attributelevel", "GREEN");
            } else {
                temp = temp.replace("attributevalue", "101.1");
                temp = temp.replace("attributethreshold", "98.1");
                temp = temp.replace("attributelevel", "YELLOW");                
            }
            message.append(temp);
        }
        if (ui.isCadenceSend1()) {
            temp = metricOnlyValue;
            temp = temp.replace("attributename", "cadence");
            temp = temp.replace("attributevalue", "22.1");
            message.append(temp);
        }
        if (ui.isSendHSSI1()) {
            temp = metricOnlyValue;
            temp = temp.replace("attributename", "hssi");
            temp = temp.replace("attributevalue", "97.1");
            message.append(temp);
        }
        if (ui.isSendHRLV1()) {
            temp = metric;
            temp = temp.replace("attributename", "hrlimitvalue");
            if (!ui.isHRLVAlert1()) {
                temp = temp.replace("attributevalue", "98.1");
                temp = temp.replace("attributethreshold", "100.1");
                temp = temp.replace("attributelevel", "GREEN");
            } else {
                temp = temp.replace("attributevalue", "101.1");
                temp = temp.replace("attributethreshold", "98.1");
                temp = temp.replace("attributelevel", "YELLOW");                
            }
            message.append(temp);
        }
        if (ui.isCBRSend1()) {
            temp = metricOnlyValue;
            temp = temp.replace("attributename", "caloricburnrate");
            temp = temp.replace("attributevalue", "22.1");
            message.append(temp);
        }
        if (ui.isSendFall1()) {
            temp = metricNoValue;
            temp = temp.replace("attributename", "fall");
            temp = temp.replace("attributelevel", "RED");
            message.append(temp);
        }
        if (ui.isSendTap1()) {
            temp = metricNoValue;
            temp = temp.replace("attributename", "tap");
            temp = temp.replace("attributelevel", "RED");
            message.append(temp);
        }
        if (ui.isSendSOC1()) {
            temp = metricNoXhold;
            temp = temp.replace("attributename", "stateofcharge");
            if (!ui.isSOCAlert1()) {
                temp = temp.replace("attributevalue", "33");
                temp = temp.replace("attributelevel", "GREEN");
            } else {
                temp = temp.replace("attributevalue", "10");
                temp = temp.replace("attributelevel", "YELLOW");                
            }
            message.append(temp);
        }
        if (ui.isSendNoMovement1()) {
            temp = metricNoValue;
            temp = temp.replace("attributename", "nomovement");
            temp = temp.replace("attributelevel", "RED");
            message.append(temp);
        }        
        if (ui.isSendHRMA1()) {
            temp = metricNoValue;
            temp = temp.replace("attributename", "heartrateminusage");
            temp = temp.replace("attributelevel", "RED");
            message.append(temp);
        }

        if (ui.isSendGPS1()) {
            temp = point.replace("latitude", user1Lat.toString());
            temp = temp.replace("longitude", user1Lon.toString());
            message.append(temp);
        }

        message.append(readingEnd); 

        message.append(detailEnd).append(eventEnd); 
        return message.toString();
    }
                
    private String buildMessage2 (Integer index) {
        StringBuilder message = new StringBuilder ();
        // user 2
        String uid = "BIOTRAC-3234" + index.toString();
        String temp = header.replace("messageuid", uid);
        temp = temp.replace("messagetime", buildTime());
        temp = temp.replace("starttime", buildTime());
        temp = temp.replace("staletime", buildStaleTime());
        message.append(temp);
        // point
        temp = point.replace("latitude", user2Lat.toString());
        temp = temp.replace("longitude", user2Lon.toString());
        message.append(temp);
        Random random = new Random ();
        if (ui.isMotionSelected2()) {
            // update lat and lon
            user2Lat += user2DeltaLat + (random.nextDouble() - 0.5)/10000.0;
            if (user2Lat > maxLat || user2Lat < minLat) {
                user2DeltaLat = -user2DeltaLat;
            }
            // update lat and lon
            user2Lon += user2DeltaLon+ (random.nextDouble() - 0.5)/10000.0;
            if (user2Lon > maxLon || user2Lon < minLon) {
                user2DeltaLon = -user2DeltaLon;
            }
        }
        // detail
        message.append (detailStart);
        message.append (readingStart);
        // reading
        // time
        temp = time;
        temp = temp.replace("eventtime", buildTime());
        message.append (temp);
        // name
        temp = name;
        temp = temp.replace("username", "Barney Rubble");
        message.append (temp);
        // metrics
        if (ui.isSendExertion2()) {
            temp = metric;
            temp = temp.replace("attributename", "exertion");
            if (!ui.isExertionAlert2()) {
                temp = temp.replace("attributevalue", "55.1");
                temp = temp.replace("attributethreshold", "95.1");
                temp = temp.replace("attributelevel", "GREEN");
            } else {
                temp = temp.replace("attributevalue", "97.1");
                temp = temp.replace("attributethreshold", "95.1");
                temp = temp.replace("attributelevel", "RED");                
            }
            message.append(temp);
        }
        if (ui.isSendHeartrate2()) {
            temp = metric;
            temp = temp.replace("attributename", "heartrate");
            if (!ui.isHeartrateAlert2()) {
                temp = temp.replace("attributevalue", "77.1");
                temp = temp.replace("attributethreshold", "180.1");
                temp = temp.replace("attributelevel", "GREEN");
            } else {
                temp = temp.replace("attributevalue", "185.1");
                temp = temp.replace("attributethreshold", "180.1");
                temp = temp.replace("attributelevel", "RED");                
            }
            message.append(temp);
        }
        if (ui.isSendCoretemp2()) {
            temp = metric;
            temp = temp.replace("attributename", "coretemp");
            if (!ui.isCoretempAlert2()) {
                temp = temp.replace("attributevalue", "98.1");
                temp = temp.replace("attributethreshold", "100.1");
                temp = temp.replace("attributelevel", "GREEN");
            } else {
                temp = temp.replace("attributevalue", "101.1");
                temp = temp.replace("attributethreshold", "98.1");
                temp = temp.replace("attributelevel", "YELLOW");                
            }
            message.append(temp);
        }
        if (ui.isCadenceSend2()) {
            temp = metricOnlyValue;
            temp = temp.replace("attributename", "cadence");
            temp = temp.replace("attributevalue", "22.1");
            message.append(temp);
        }
        if (ui.isSendHSSI2()) {
            temp = metricOnlyValue;
            temp = temp.replace("attributename", "hssi");
            temp = temp.replace("attributevalue", "98.1");
            message.append(temp);
        }
        if (ui.isSendHRLV2()) {
            temp = metric;
            temp = temp.replace("attributename", "hrlimitvalue");
            if (!ui.isHRLVAlert2()) {
                temp = temp.replace("attributevalue", "98.1");
                temp = temp.replace("attributethreshold", "100.1");
                temp = temp.replace("attributelevel", "GREEN");
            } else {
                temp = temp.replace("attributevalue", "101.1");
                temp = temp.replace("attributethreshold", "98.1");
                temp = temp.replace("attributelevel", "YELLOW");                
            }
            message.append(temp);
        }
        if (ui.isCBRSend2()) {
            temp = metricOnlyValue;
            temp = temp.replace("attributename", "caloricburnrate");
            temp = temp.replace("attributevalue", "22.1");
            message.append(temp);
        }
        if (ui.isSendFall2()) {
            temp = metricNoValue;
            temp = temp.replace("attributename", "fall");
            temp = temp.replace("attributelevel", "RED");
            message.append(temp);
        }
        if (ui.isSendTap2()) {
            temp = metricNoValue;
            temp = temp.replace("attributename", "tap");
            temp = temp.replace("attributelevel", "RED");
            message.append(temp);
        }
        if (ui.isSendSOC2()) {
            temp = metricNoXhold;
            temp = temp.replace("attributename", "stateofcharge");
            if (!ui.isSOCAlert2()) {
                temp = temp.replace("attributevalue", "33");
                temp = temp.replace("attributelevel", "GREEN");
            } else {
                temp = temp.replace("attributevalue", "10");
                temp = temp.replace("attributelevel", "YELLOW");                
            }
            message.append(temp);
        }
        if (ui.isSendNoMovement2()) {
            temp = metricNoValue;
            temp = temp.replace("attributename", "nomovement");
            temp = temp.replace("attributelevel", "RED");
            message.append(temp);
        }
        if (ui.isSendHRMA2()) {
            temp = metricNoValue;
            temp = temp.replace("attributename", "heartrateminusage");
            temp = temp.replace("attributelevel", "RED");
            message.append(temp);
        }

        if (ui.isSendGPS2()) {
            temp = point.replace("latitude", user2Lat.toString());
            temp = temp.replace("longitude", user2Lon.toString());
            message.append(temp);
        }

        message.append(readingEnd); 

        message.append(detailEnd).append(eventEnd); 
        return message.toString();
    }
            
    private String buildTime () {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        calendar.setTime(new Date ());
        SimpleDateFormat format = new SimpleDateFormat (DTFORMAT);
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        String dateString = format.format(calendar.getTime());
        return dateString;
    }
    
    private String buildStaleTime () {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        //calendar.setTimeZone(TimeZone.getTimeZone("GMT"));
        calendar.setTime(new Date ());
        calendar.add(Calendar.MILLISECOND, 10000);
        SimpleDateFormat format = new SimpleDateFormat (DTFORMAT);
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        String dateString = format.format(calendar.getTime());
        return dateString;
    }

    Thread continuousThread;
    
    public Boolean startContinuousSend () {
        if (continuousThread == null) {
            continuousThread = new Thread (this);
            continuousThread.start();
            return true;
        }
        return false;
    }

    public void stopContinuousSend() {
        if (continuousThread != null) {
            if (!continuousThread.isAlive()) {
                continuousThread.interrupt();
            }
        }
        continuousThread = null;
    }
    
    @Override
    public void run() {
        while (ui.isContinuousSend()) {
            buildAndSend ();
            try {
                Thread.sleep(10000);
            } catch (InterruptedException ex) {
                System.out.println ("Thread interrupted");
            }
        }
    }
}
