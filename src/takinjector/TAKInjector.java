/*
 * TAKInjector.java
 *
 * Copyright (c) 2001-2022 Domenix Corporation Dba Relevant Technology Inc
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD) 
 *
 * Connect to and send data to TAK Server
 *
 */
package takinjector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Connect to and send data to TAK Server
 * @author jmerritt
 */
public class TAKInjector {
    Socket clientSocket;
    PrintWriter writer;
    BufferedReader in;
    
    public Boolean connect(String ip, String port) {
        System.out.println ("Trying to connect to TAK Server: " + ip + " " + port);
        try {
            clientSocket = new Socket (ip, Integer.valueOf(port));
            writer = new PrintWriter (clientSocket.getOutputStream (), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException ex) {
            System.out.println (ex.getMessage());
            return (false);
        }
        System.out.println("Connection successful");
        return(true);
    }

    public Boolean send(String text) {
        if (writer != null) {
            writer.println(text);
            System.out.println("Writing: " + text);
            return true;
        }
        return false;
    }

    public void receive() {
        if (in != null) {
            System.out.println("Receiving");
            String text;
            try {
                while ((text = in.readLine()) != null) {
                    System.out.println("Text received: " + text);
                }
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }
    public void disconnect () {
        try {
            if (clientSocket != null)
            {
                clientSocket.close();
            }
        } catch (IOException ex) {
            System.out.println (ex.getMessage());
            return;
        }
        System.out.println ("Disconnect successful");
    }
}
